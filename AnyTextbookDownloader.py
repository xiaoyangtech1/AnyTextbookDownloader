import os
import sys
import time
import requests
from bs4 import BeautifulSoup
import tkinter as tk
from tkinter import filedialog, messagebox, ttk
import threading
import re
from datetime import datetime
import json
import webbrowser
import fitz
import tkinter.font as tkfont
from DrissionPage import ChromiumPage,SessionPage,ChromiumOptions
import urllib.parse
import configparser
from tkinter import BooleanVar

# 初始化主窗口，定宽
root = tk.Tk()

#接下来是初始化部分，包括检查版本、创建日志文件等
current_version = "4.0.1005" #版本号配置，用于检查更新等
channel = "Official" #渠道配置，用于区分不同版本



log_file_path = os.path.join(os.getcwd(), "logs", datetime.now().strftime('%Y-%m-%d-%H-%M-%S') + "-log.txt")
if os.name == 'nt':  # Windows
    download_directory = r"C:\TextbookDownloads"  # 默认下载目录为C盘的AnyTextbookDownloader文件夹
else:  # macOS, Linux
    download_directory = os.getcwd()


# 检查日志目录是否存在，如果不存在则创建
log_directory = os.path.dirname(log_file_path)
if not os.path.exists(log_directory):
    os.makedirs(log_directory)
def log_message(message):
    with open(log_file_path, "a", encoding="utf-8") as log_file:
        log_file.write(f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')} - {message}\n")

log_message(f"下载目录：{download_directory}，日志存放目录：{log_file_path}")

# 设置中心
# 初始化设置相关变量
config_file_path = os.path.join(os.getcwd(), 'settings.ini')

# 创建 Tkinter 的 BooleanVar 对象
show_update_reminder = BooleanVar()
show_notice = BooleanVar()

# 读取配置文件
config = configparser.ConfigParser()
config.read(config_file_path)

# 如果配置文件不存在，使用默认值
if not config.has_section('Settings'):
    config.add_section('Settings')
    config.set('Settings', 'ShowUpdateReminder', 'True')
    config.set('Settings', 'ShowNotice', 'True')

# 通过配置文件设置变量
show_update_reminder.set(config.getboolean('Settings', 'ShowUpdateReminder', fallback=True))
show_notice.set(config.getboolean('Settings', 'ShowNotice', fallback=True))

# 保存设置到 ini 文件
def save_settings():
    # 确保将布尔值转换为字符串形式
    config.set('Settings', 'ShowUpdateReminder', str(show_update_reminder.get()))
    config.set('Settings', 'ShowNotice', str(show_notice.get()))
    with open(config_file_path, 'w') as configfile:
        config.write(configfile)

# 创建设置中心窗口
def open_settings():
    settings_window = tk.Toplevel(root)
    settings_window.title("设置中心")
    settings_window.geometry("400x400")  # 设置默认大小
    # 是否显示更新提示复选框
    update_checkbox = tk.Checkbutton(settings_window, text="显示更新提示", variable=show_update_reminder)
    update_checkbox.pack(pady=10, padx=20)

    # 是否显示系统公告复选框
    notice_checkbox = tk.Checkbutton(settings_window, text="显示系统公告", variable=show_notice)
    notice_checkbox.pack(pady=10, padx=20)

    # 保存按钮
    save_button = tk.Button(settings_window, text="保存", command=lambda: [save_settings(), settings_window.destroy()])
    save_button.pack(pady=20)

def show_system_notice():
    if not show_notice.get():
        log_message("用户选择不显示系统公告")
        return
    try:
        response = requests.get("https://yifang.yxxblog.top/api/atd-notice")
        response.raise_for_status()
        response.encoding = response.apparent_encoding  # 设置正确的编码
        notice_html = response.text

        # 使用 BeautifulSoup 解析 HTML 内容
        soup = BeautifulSoup(notice_html, 'html.parser')
        notice_text_content = soup.get_text()

        # 创建一个独立窗口显示系统公告
        notice_window = tk.Toplevel(root)
        notice_window.title("系统公告-右上角可以关闭")
        
        # 设置字体
        if os.name == 'nt':  # Windows
            font_family = "微软雅黑"
        else:  # macOS, Linux
            try:
                font_family = "WenQuanYi Micro Hei"
                custom_font = tkfont.Font(family=font_family, size=12)
                test_label = tk.Label(notice_window, text="test", font=custom_font)
                test_label.destroy()
            except tk.TclError:
                font_family = "Arial"

        custom_font = tkfont.Font(family=font_family, size=12)

        notice_text = tk.Text(notice_window, wrap=tk.WORD, font=custom_font)
        notice_text.pack(pady=10, padx=10)
        notice_text.insert(tk.END, notice_text_content)
        notice_text.config(state=tk.DISABLED)
        width = 400  # 默认宽度
        height = 300  # 默认高度
        notice_window.geometry(f"{width}x{height}")

    except requests.RequestException as e:
        log_message(f"请求系统公告失败: {e}")
        messagebox.showwarning("系统公告", "无法获取系统公告")
def check_for_updates():
    if not show_update_reminder.get():
        log_message("用户选择不显示更新提示")
        return
    try:
        response = requests.get("https://yifang.yxxblog.top/api/update/atd-version.json")
        response.raise_for_status()
        update_info = response.json()

        if update_info["version"] != current_version:
            message = f"""
            当前版本: {current_version}
            最新版本: {update_info["version"]}
            发布时间: {update_info["time"]}
            更新描述: {update_info["describe"]}
            建议立刻下载更新，以保证程序正常运行。
            是否前往下载最新版本？
            """
            if messagebox.askyesno("版本更新", message):
                webbrowser.open_new_tab(update_info["downloadURL"])
    except requests.RequestException as e:
        log_message(f"检查更新失败: {e}")

def open_about():
    # 创建一个独立窗口
    about_window = tk.Toplevel(root)
    about_window.title("关于 亿方教材助手")

    # 创建文本框
    about_text = tk.Text(about_window, wrap=tk.WORD)
    about_text.pack(pady=10, padx=10)

    # 在文本框中插入关于信息
    about_info = f"""
    亿方教材助手 V{current_version}
    CH4NGE大版本-Python跨平台版
    版权所有 © 2024 转载推荐时请注明开源项目地址
    特别感谢 @玄离199
    我们利用有限的时间开发了这个软件帮助用户下载各种课本资源，希望能帮到你。
    软件使用过程中遇到问题请联系哔哩哔哩UP主“小杨聊科技”。
    更多信息请访问软件官网。
    操作系统名称：{os.name}
    程序更新渠道：{channel}
    """
    about_text.insert(tk.END, about_info)
    about_text.config(state=tk.DISABLED)  # 设置文本框为只读
    about_window.geometry("800x600")  # 设置默认大小


# 菜单栏中功能初始化

def open_download_directory():
    if os.name == 'nt':  # Windows
        os.startfile(download_directory)
    elif os.name == 'posix':  # macOS, Linux
        os.system(f'open "{download_directory}"' if sys.platform == 'darwin' else f'xdg-open "{download_directory}"')

def pdf_to_png():
    file_path = filedialog.askopenfilename(filetypes=[("PDF文件", "*.pdf")])
    if not file_path:
        return

    output_dir = filedialog.askdirectory()
    if not output_dir:
        return

    doc = fitz.open(file_path)
    base_filename = os.path.splitext(os.path.basename(file_path))[0]

    # 创建一个新窗口显示进度条
    progress_window = tk.Toplevel(root)
    progress_window.title("导出进度")
    progress_label = tk.Label(progress_window, text="正在导出...")
    progress_label.pack(padx=20, pady=10)
    progress_bar = ttk.Progressbar(progress_window, length=300, mode="determinate", maximum=len(doc))
    progress_bar.pack(padx=20, pady=10)

    def export_pages():
        for i in range(len(doc)):
            page = doc.load_page(i)  # 0-based index
            pix = page.get_pixmap(matrix=fitz.Matrix(5, 5))
            pix.save(os.path.join(output_dir, f"{base_filename}_{i+1}.png"))
            progress_bar["value"] = i + 1
            progress_label.config(text=f"正在导出：{i+1}/{len(doc)}")
            progress_window.update()
        progress_label.config(text="导出完成")
        messagebox.showinfo("完成", "PDF 已成功导出为 PNG 图片")
        progress_window.destroy()

    threading.Thread(target=export_pages).start()
# 添加合并多行文本为一行
def merge_lines():
    # 创建一个独立窗口
    merge_window = tk.Toplevel(root)
    merge_window.title("多行文本合并一行")
    merge_window.geometry("400x400")  # 设置窗口大小

    # 创建文本框和按钮
    text_area = tk.Text(merge_window, height=10, width=50)
    text_area.pack(pady=10)
    merge_button = tk.Button(merge_window, text="合并", command=lambda: merge_text(text_area))
    merge_button.pack(pady=10)

def merge_text(text_widget):
    # 获取文本框中的所有文本
    text = text_widget.get("1.0", tk.END)
    # 移除换行符并合并为一行
    merged_text = text.replace("\n", " ")
    # 清空文本框并写入合并后的文本
    text_widget.delete("1.0", tk.END)
    text_widget.insert("1.0", merged_text)

#界面初始化
def browse_directory():
    global download_directory
    # 弹出文件选择框让用户选择保存目录
    folder_path = filedialog.askdirectory()
    # 如果用户选择了一个新的目录，那么更新下载目录
    if folder_path != "":
        download_directory = folder_path
    # 清空文本框并插入新的下载目录
    directory_entry.delete(0, tk.END)
    directory_entry.insert(0, download_directory)
def add_right_click_menu(entry_widget):
    # 创建右键菜单
    menu = tk.Menu(entry_widget, tearoff=0)
    menu.add_command(label="复制", command=lambda: entry_widget.event_generate('<<Copy>>'))
    menu.add_command(label="粘贴", command=lambda: entry_widget.event_generate('<<Paste>>'))
    menu.add_command(label="剪切", command=lambda: entry_widget.event_generate('<<Cut>>'))
    menu.add_command(label="删除", command=lambda: entry_widget.delete(0, tk.END))

    # 绑定右键单击事件
    def show_menu(event):
        menu.post(event.x_root, event.y_root)
    entry_widget.bind("<Button-3>", show_menu)

#程序核心功能正式开始定义
def download_book():
    # 获取用户输入的URL
    book_url = url_entry.get()
    if "basic.smartedu.cn" in book_url:  # 判断是否为智慧教育平台下载链接
        download_smartedu_content(book_url)
    elif "book.pep.com.cn" in book_url:  # 判断是否为人教数字教材下载链接
        download_pep_content(book_url)
    else:
        messagebox.showerror("提示", "当前内容不支持下载")

# # 定义select_browser_path函数
# def select_browser_path():
#     # 创建一个新窗口
#     path_window = tk.Toplevel(root)
#     path_window.title("选择浏览器路径")
#     path_window.geometry("400x150")

#     # 添加提示信息标签
#     info_label = tk.Label(path_window, text="请输入浏览器路径，路径查看方法请点击下方“帮助”按钮查看")
#     info_label.grid(row=0, column=0, columnspan=2, padx=10, pady=10, sticky='w')

#     # 输入路径的文本框
#     path_entry = tk.Entry(path_window, width=50)
#     path_entry.grid(row=1, column=0, columnspan=2, padx=10, pady=5, sticky='w')

#     # 定义设置浏览器路径的函数
#     def set_browser_path():
#         path = path_entry.get()
#         if path:
#             #保存到config.json里面，不存在则创建
#             with open("config.json", "w") as config_file:
#                 config = {"browser_path": path}
#                 json.dump(config, config_file)
#             messagebox.showinfo("提示", "浏览器路径设置成功")
#             path_window.destroy()

#     # “确定”按钮
#     confirm_button = tk.Button(path_window, text="确定", command=set_browser_path, width=10)
#     confirm_button.grid(row=2, column=0, padx=10, pady=20, sticky='w')

#     # “帮助”按钮
#     help_button = tk.Button(path_window, text="帮助", command=lambda: webbrowser.open_new_tab("https://xiaoyangtech.feishu.cn/wiki/E6RFwJvJhiblK3ketcncCNZCnAe"), width=10)
#     help_button.grid(row=2, column=1, padx=5, pady=20, sticky='w')

#接下来是玄离199贡献的解决方案，动态获取x-nd-auth解决反爬虫机制
def parse_auth_field(url):
    parsed_url = urllib.parse.urlparse(url)
    query_params = urllib.parse.parse_qs(parsed_url.query)
    if 'headers' in query_params:
        headers_str = urllib.parse.unquote(query_params['headers'][0])
        headers_json = json.loads(headers_str)
        if 'X-ND-AUTH' in headers_json:
            return headers_json['X-ND-AUTH']
    return None
# 定义智慧教育平台登录函数
def login_smartedu():
    # 初始化 Tkinter 主窗口
    root = tk.Tk()
    root.withdraw()  # 隐藏主窗口
    # 弹出提示信息
    messagebox.showinfo("提示", "接下来请登录账号，登录完毕后请等待程序自动处理\nTips：如果无法打开登录窗口，请安装谷歌浏览器\n如果登录后程序无响应，请关闭IDM等下载器的浏览器插件")
    # 初始化浏览器
    # if os.path.exists('user_config.json'):  # 检查用户配置文件
    #     with open('user_config.json', 'r', encoding='utf-8') as config_file:
    #         config = json.load(config_file)
    #         browser_path = config.get('browser_path')
    #         co = ChromiumOptions().set_browser_path(browser_path)
    #         page = ChromiumPage(addr_or_opts=co)
    # else:
    #     try:
    #         page = ChromiumPage()  # 用户无配置文件，默认方式打开浏览器
    #     except FileNotFoundError:
    #         messagebox.showerror("错误", "未找到浏览器，请前往文件菜单选择浏览器路径")
    try:
        page = ChromiumPage()
    except FileNotFoundError:
            messagebox.showerror("错误", "未找到浏览器，请前往安装谷歌浏览器")
    # 打开登录页面
    page.get('https://auth.smartedu.cn/uias/login')
    # 循环检查登录状态
    while True:
        time.sleep(2)  # 每隔2秒检查一次
        if page.url == 'https://www.smartedu.cn/':  # 登录完成，跳转到主页
            break
    # 登录完成后，打开课本页面
    page.get('https://basic.smartedu.cn/tchMaterial/detail?contentType=assets_document&contentId=bdc00134-465d-454b-a541-dcd0cec4d86e&catalogType=tchMaterial&subCatalog=tchMaterial')
    # 开始监听包含指定文本的请求
    page.listen.start('https://basic.smartedu.cn/pdfjs/2.13/web/viewer.html?file=')
    res = page.listen.wait()  # 等待并获取一个数据包
    # 解析并获取 X-ND-AUTH 认证信息
    x_nd_auth = parse_auth_field(res.url)
    if x_nd_auth:
        with open("auth_info.txt", "w") as file:
            file.write(x_nd_auth)
        #关闭接管的浏览器
        page.close()
        messagebox.showinfo("登录成功", "智慧教育平台登录成功")
    else:
        messagebox.showerror("错误", "未能获取到认证信息，请重试")






def download_smartedu_content(url):
    if not os.path.exists("auth_info.txt"):
        messagebox.showwarning("未登录账号", "请前往文件菜单中登录智慧教育平台账号")
        return

    with open("auth_info.txt", "r") as file:
        x_nd_auth = file.read().strip()

    content_id_match = re.search(r'contentId=([^&]+)', url)
    if content_id_match:
        content_id = content_id_match.group(1)
        title_url = f"https://s-file-1.ykt.cbern.com.cn/zxx/ndrv2/resources/tch_material/details/{content_id}.json"
        response = requests.get(url=title_url)
        if response.status_code == 200:
            json_data = response.text
            title_match = re.search(r'"title"\s*:\s*"([^"]+)"', json_data)
            if title_match:
                title_tag = title_match.group(1)
                title = title_tag.strip() if title_tag else None
                sub_directory = title + " " + datetime.now().strftime('%Y-%m-%d-%H-%M-%S') if title else datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
                
                # 替换目录名中的字符串
                sub_directory = sub_directory.replace("根据2022年版课程标准修订", "旧版本教材")

                valid_sub_directory = "".join(c if c.isalnum() or c.isspace() else "_" for c in sub_directory)
                full_path = os.path.join(download_directory, valid_sub_directory)
                os.makedirs(full_path, exist_ok=True)

                # 解析 JSON 数据
                json_data = response.json()
                log_message(f"智慧云-JSON响应数据：{json_data}")

                # 提取PDF格式的资源URL
                download_urls = []
                ti_items = json_data.get("ti_items", [])
                for item in ti_items:
                    log_message(f"智慧云-PDF资源项：{item}")
                    if item.get("ti_format") == "pdf":
                        download_urls.extend(item.get("ti_storages", []))

                if not download_urls:
                    messagebox.showwarning("Warning", "没有找到PDF格式的资源")
                    log_message(f"{title}.pdf 没有找到PDF格式的资源")
                    return

            def download_pdf(download_urls, directory, filename, title):
                for download_url in download_urls:
                    decoded_url = urllib.parse.unquote(download_url)
                    if re.search(r'[\u4e00-\u9fa5]', decoded_url):
                        user_choice = messagebox.askyesno("选择版本", "检测到资源可能包含新课标教材，是否下载新版本？\n目前仅部分教材支持，如果下载失败则代表旧版本被彻底删除")
                        if not user_choice:  # 如果选择下载老版本
                            # 替换下载URL中的文件名为 pdf.pdf
                            download_url = re.sub(r'[^/]+$', 'pdf.pdf', download_url)

                    # 处理文件名替换
                    old_title = title
                    title = title.replace("根据2022年版课程标准修订", "旧版本教材")

                    response = requests.get(download_url, headers={
                        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36 Edg/126.0.0.0',
                        'Accept': '*/*',
                        'Accept-Encoding': 'gzip, deflate, br, zstd',
                        'Accept-Language': 'zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6',
                        'Dnt': '1',
                        'Origin': 'https://basic.smartedu.cn',
                        'Referer': 'https://basic.smartedu.cn/',
                        'Sec-Ch-Ua': '"Not/A)Brand";v="8", "Chromium";v="126", "Microsoft Edge";v="126"',
                        'Sec-Ch-Ua-Mobile': '?0',
                        'Sec-Ch-Ua-Platform': '"Windows"',
                        'Sec-Fetch-Dest': 'empty',
                        'Sec-Fetch-Mode': 'cors',
                        'Sec-Fetch-Site': 'cross-site',
                        'X-Nd-Auth': x_nd_auth
                    })
                    if response.status_code == 200:
                        file_path = os.path.join(directory, f"{title}.pdf")
                        with open(file_path, 'wb') as file:
                            file.write(response.content)
                        log_text.insert(tk.END, f"{old_title}.pdf 下载完成\n")
                        log_message(f"{old_title}.pdf 下载完成\n")
                        return True
                    else:
                        log_text.insert(tk.END, f"{old_title}.pdf 下载失败，尝试下一个链接\n")
                return False

            def download_all_pdfs():
                success = False
                for i, download_url in enumerate(download_urls):
                    log_text.insert(tk.END, f"正在下载：{title}.pdf ({i+1}/{len(download_urls)})\n")
                    success = download_pdf([download_url], full_path, f"{title}_{i+1}", title)
                    log_text.update_idletasks()
                    if success:
                        break
                if not success:
                    log_text.insert(tk.END, f"{title}.pdf 所有链接均无法下载\n")
                    log_message(f"智慧云-{title}.pdf 所有链接均无法下载\n")
                    log_window.after(2000, log_window.destroy)
                    if messagebox.askyesno("下载失败", "所有链接均无法下载,请尝试重新登录账号。\n如果下载的是老版本教材，那么有可能是教材不存在旧版\n如果确信已登录，是否报告BUG？"):
                        webbrowser.open("https://wj.qq.com/s2/14921146/3b02/")

            # 创建一个新窗口显示下载日志
            log_window = tk.Toplevel(root)
            log_window.title("下载日志")
            log_window.geometry("400x300")
            log_text = tk.Text(log_window, height=15, width=50)
            log_text.pack(padx=10, pady=10)
            log_button = tk.Button(log_window, text="关闭", command=log_window.destroy)
            log_button.pack(pady=10)

            threading.Thread(target=download_all_pdfs).start()
        else:
            messagebox.showwarning("Warning", "无法获取教材信息")
    else:
        messagebox.showwarning("Warning", "无效的URL")
    
def download_pep_content(url):
    messagebox.showinfo("提示", "由于人教社更新接口，目前暂时关闭人教社教材下载，请期待后续更新")


# 在窗口标题中引用版本号变量
root.title(f"亿方教材助手 V{current_version}")
root.geometry("460x200")
root.resizable(False, False)

# 在主窗口初始化后调用show_system_notice函数和check_for_updates函数
root.after(100, show_system_notice)

root.after(500, check_for_updates)

url_label = tk.Label(root, text="请输入教材链接，文件菜单可以快速打开教材列表：")
url_label.grid(row=0, column=0, sticky='w', padx=5, pady=5)

url_entry = tk.Entry(root, width=55)
url_entry.grid(row=1, column=0, sticky='ew', padx=5, pady=5)

# 添加右键菜单功能
add_right_click_menu(url_entry)

directory_label = tk.Label(root, text="请选择下载目录：")
directory_label.grid(row=2, column=0, sticky='w', padx=5, pady=5)

directory_entry = tk.Entry(root, width=55)
directory_entry.insert(0, download_directory)
directory_entry.grid(row=3, column=0, sticky='ew', padx=5, pady=5)

button_frame = tk.Frame(root)
button_frame.grid(row=4, column=0, sticky='w')

browse_button = tk.Button(button_frame, text="浏览", command=browse_directory, width=10)
browse_button.pack(side='left', padx=5, pady=5)

download_button = tk.Button(button_frame, text="下载", command=download_book, width=10)
download_button.pack(side='left', padx=5, pady=5)

# 在主窗口中添加菜单栏
menu_bar = tk.Menu(root)
root.config(menu=menu_bar)

# 创建文件菜单
file_menu = tk.Menu(menu_bar, tearoff=0)
menu_bar.add_cascade(label="文件", menu=file_menu)
file_menu.add_command(label="打开中小学教材列表", command=lambda: webbrowser.open_new_tab("https://basic.smartedu.cn/tchMaterial"))
file_menu.add_command(label="打开首选项设置中心", command=open_settings)
file_menu.add_command(label="登录中小学云平台账号", command=login_smartedu)
# file_menu.add_command(label="手动选择浏览器路径", command=select_browser_path)
file_menu.add_command(label="打开当前教材下载目录", command=open_download_directory)
file_menu.add_command(label="清除教材助手日志文件", command=lambda: os.remove(log_file_path))
file_menu.add_command(label="关闭教材助手软件窗口", command=root.destroy)


# 创建学科工具菜单
subject_tools_menu = tk.Menu(menu_bar, tearoff=0)
menu_bar.add_cascade(label="学科工具", menu=subject_tools_menu)
subject_tools_menu.add_command(label="思维导图制作", command=lambda: webbrowser.open_new_tab("https://mubu.com/home"))
subject_tools_menu.add_command(label="人工智能问答", command=lambda: webbrowser.open_new_tab("https://hailuoai.com"))
subject_tools_menu.add_command(label="幻灯片模板", command=lambda: webbrowser.open_new_tab("https://www.officeplus.cn"))
subject_tools_menu.add_command(label="语文字词表", command=lambda: webbrowser.open_new_tab("https://hanyu.baidu.com/hanyu-page/knowledge/textbook/list"))
subject_tools_menu.add_command(label="古诗文搜索", command=lambda: webbrowser.open_new_tab("https://www.gushiwen.cn"))
subject_tools_menu.add_command(label="中国大事记", command=lambda: webbrowser.open_new_tab("https://www.gov.cn/guoqing/lishi.htm"))

# 创建排版菜单
formatting_menu = tk.Menu(menu_bar, tearoff=0)
menu_bar.add_cascade(label="排版及印刷", menu=formatting_menu)
formatting_menu.add_command(label="多行文本合并一行", command=merge_lines)
formatting_menu.add_command(label="PDF去水印", command=lambda: webbrowser.open_new_tab("https://www.pdf365.cn/del-watermark"))
formatting_menu.add_command(label="PDF转图片", command=pdf_to_png)

# 创建帮助菜单
help_menu = tk.Menu(menu_bar, tearoff=0)
menu_bar.add_cascade(label="帮助", menu=help_menu)
help_menu.add_command(label="使用教程", command=lambda: webbrowser.open_new_tab("https://www.bilibili.com/video/BV1W2421M7pV/"))
help_menu.add_command(label="软件官网", command=lambda: webbrowser.open_new_tab("https://yifang.yxxblog.top"))
help_menu.add_command(label="关于", command=open_about)

root.mainloop()